from io import open

# Ruta donde leeremos el fichero, r indica lectura (por defecto ya es r)
fichero = open('notasAlumnos.txt','r+')

# Lectura completa
texto = fichero.readlines()
texto[1] = "Juan Flores;333;8\n"
fichero.seek(0)
fichero.writelines(texto)
# Cerramos el fichero
fichero.close()

for linea in texto:
    x = linea.split(";")
    print (x)
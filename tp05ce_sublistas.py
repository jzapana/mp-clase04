from operator import itemgetter, attrgetter

# Lista utilizando listas anidadas 
lista = [[5,'Mary', 10.0], [2,'Anna', 2.0], [4, 'Juan', 7.5], [1, 'Pedro',6.5]]

listaOrdenada = sorted(lista,key=itemgetter(0))
print(listaOrdenada)

listaOrdenada = sorted(lista,key=itemgetter(1))
print(listaOrdenada)

listaOrdenada = sorted(lista,key=itemgetter(2))
print(listaOrdenada)
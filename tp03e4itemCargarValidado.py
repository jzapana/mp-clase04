"""Cargar  las  notas  hasta  que  el operador  no  quiera  ingresar más  datos,  
validar  que  las  notas  estén  en el intervalo [1, 10]. 
Si el valor ingresado NO es válido mostrar un mensaje de error y volver a solicitarlo
 hasta que el valor sea correcto. 
"""
import os

def validado(nota):
    ok = False
    if (nota >= 1) and (nota <= 10):
        ok = True
    else:
        print('No validado, fuera de rango')
    return ok

def cargarLista():
    lista = list()
    item =input('Ingrese valor (vacío finalizar): ')
    while (item !=''):
        if item.isnumeric(): 
            itemNumerico = float(item)
            if validado(itemNumerico):
                lista.append(itemNumerico)
        else:
            print('No es un número válido')
        item =input('Ingrese valor (vacío finalizar): ')
    return lista
     
#principal
os.system('clear') # En Windows es cls en vez de clear (macOs)

lista = cargarLista()
print(lista)